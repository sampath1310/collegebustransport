import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class Routecount implements ActionListener{
   private JFrame frame =new JFrame();
   private JLabel label;
   private JPanel panel;
   private JButton btnproc; 	
   private Routecount(){
	   window1();
   }
   public void window1(){
	   frame =new JFrame("Routecount");
	   frame.setSize(600,600);/*Frame size is 600x600 size ,size is fixed for present window */
	   frame.setResizable(false);
	   frame.setLocationRelativeTo(null);/*window will pop up at center fo screen*/
	   frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	   
	   intro();
	      }
   private void intro(){
	   String intro= "<html><h1>Application Introduction</h1>" + 
		        "<p><ul><li>This application will provide information of passenger at a particular stop.</li></p>" +
			    "<li>Google Maps will help you trace out the route and topography of area</li>" + 
		        "<li>This application will help you maintain records of passengers ,their departure and arrivel</li></ul>" + 
		        "<h2>Application Insight</h2>" + 
		        "<p>Applications uses JDBC for maintaining data base and records.This application will " + 
		        "provide you all the information of passengers at a particular stop.This application is created using java,</p>" + 
		       	"for graphical user interface(GUI) Swing,awt packages were used."	+    
		    
		        "</html>";
	            label=new JLabel(intro);
	            label.setBounds(15, 135, 584, 245);
	            label.setAlignmentY(0.0f);
	            panel=new JPanel();  
	            panel.setLayout(null);
	            panel.add(label);	  
	            panel.setBackground(Color.BLACK);  
	            label.setForeground(Color.WHITE);
	            frame.getContentPane().add(panel);   	     
	            btnproc = new JButton("Proceed");
	            btnproc.addActionListener(this);
	            btnproc.setBounds(229, 460, 89, 23);
	            panel.add(btnproc);   
	   }
   public void actionPerformed(ActionEvent e){
	   Object act1=e.getSource();       
	   if(act1==btnproc){
		  
		   new Page2();
		  		 
	   }
   }
   public static void main(String args[]){
	   EventQueue.invokeLater(new Runnable(){
	   public void run(){
		   new Routecount().frame.setVisible(true);
	   }
	   });
	   
   }
  }