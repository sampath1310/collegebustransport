import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
public class Page22 {
	private JComboBox<String> busroute;
	private JComboBox<String> jc;
    private JFrame frame2;
    private JPanel toppanel;
    private JPanel panel1;
    private JPanel panel2;
    private JTabbedPane tabbedpane;
    private JLabel count,finalcount;
    static String text,finalcountstr,finalcounttext;
    private Browser browser = new Browser();
    Database db;
    
    
     public Page22(){
    	 frame2 = new JFrame("Route Count");    	
    	 frame2.setSize(600,600);
      	 frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	 frame2.setLocationRelativeTo(null);    	    
    	 window2();
    	 frame2.setVisible(true);
    	 }
     protected void window2(){
    	 toppanel = new JPanel();
    	 toppanel.setLayout(new BorderLayout());
    	 frame2.getContentPane().add(toppanel);
    	 createpanel2(); 

    	 createpanel1();
    	 tabbedpane = new JTabbedPane();
    	 tabbedpane.addTab( "Option", panel1 );  	 
    	 
    	  tabbedpane.addTab( "Map", panel2 );	
		 toppanel.add( tabbedpane, BorderLayout.CENTER );            	 
     }

     private void createpanel2(){    	 
    	 panel2 = new JPanel();
    	// final Browser browser = new Browser();
         BrowserView browserView = new BrowserView(browser);
          browser.loadURL("file://C:/Users/sampa/workspace/CollegeBusTransport/index.html");  


             panel2.setLayout(new BorderLayout());
           panel2.add(browserView,BorderLayout.CENTER);       
    	    	 
     }  
     private void createpanel1(){    	 
    	 panel1 = new JPanel();
    	 panel1.setLayout(null);   	     	
    	 panel1.setBackground(Color.WHITE);   
    	 
    	 String panelhead="<html><h1>Select required option for output</h1></html>";
    	
    	
    	 JLabel label=new JLabel(panelhead);
    	 
    	 label.setBounds(22,37,449,58);
    	 panel1.add(label);  	    	 
    	 Jcombofill();
    	
     }
    private void Jcombofill(){
    	 	      		 
	 Border border=BorderFactory.createTitledBorder("Any");	 
	 panel1.setBorder(border);	 
	 /*   Creating new panel to separate the options and display and */
	 
	 
	
	 JPanel satpanel=new JPanel();
	 satpanel.setSize(170, 378);
 	 satpanel.setLocation(10, 144);
	 satpanel.setLayout(null);
	 Border opt=BorderFactory.createTitledBorder("Options");
	 satpanel.setBorder(opt);
	 //Bus Choice Option
	 String bustr="<html><p>Select route number of bus</html><p>";
	 JLabel bus=new JLabel(bustr);	    
	 bus.setBounds(0, 34, 160, 26);	
	 satpanel.add(bus);	
	 //adding combobox to panel
	 busroute = new JComboBox<String>();	
	
 	 busroute.setBounds(30, 71, 112, 25);
	 jc = new JComboBox<String>();	 
	 satpanel.add(busroute);
	 jc.setVisible(false);
	 jc.setBounds(30, 164, 112, 20);
	 satpanel.add(jc);	 
	 //Stop Option
	 String stopstr="<html><p>Select stop from list</html><p>";  
	 JLabel stop=new JLabel(stopstr);   	 
	 stop.setVisible(false);	
	 stop.setBounds(0,118,160,26);	
	 satpanel.add(stop); 
	 /* Creating Object for Database class and utilizing methods present in it */ 
	  db=new Database(); 		  
	 	
	busroute.addItem("Select routeno");
	 for(int i=0;i<db.getRouteno().size();i++){
		 busroute.addItem(db.getRouteno().get(i).toString());
		 }
		  
     busroute.addItemListener(new ItemListener(){
    		   public void itemStateChanged(ItemEvent e){
    			   if(e.getStateChange()==2){    				   
    				   jc.removeAllItems();
    				    stop.setVisible(true);    					
    				   jc.setVisible(true);
    				   jc.addItem("Select Stop");
    				   for(int i=0;i<db.getStop((String) busroute.getSelectedItem()).size();i++){
    					   jc.addItem(db.getStop((String) busroute.getSelectedItem()).get(i).toString());
    				              }   				  
    			 		   }
    		   }
    	 });
    	 
 	    String contstr="Selected stop:" ;	
 	 	 /*Creating new panel for Displaying Count of Passenger */
	    JPanel 	dispanel=new JPanel();    	
	    dispanel.setLayout(null);
	    dispanel.setBounds(190,144,379,378);
	    //Adding label to display panel
	    Border disp=BorderFactory.createTitledBorder("Display");
	    dispanel.setBorder(disp);
	    count= new JLabel(contstr);	
	    count .setBounds(10, 62, 359, 63);
	    dispanel.add(count);	    
	    /*Creating label to display the number of Passenger at a particular stop or selected stop*/
	    finalcountstr="Number of passenger : ";
	    finalcount = new JLabel(finalcountstr);
	    finalcount.setBounds(10,136,359,55);
	    dispanel.add(finalcount);
     	 jc.addItemListener(new ItemListener(){
  		   public void itemStateChanged(ItemEvent e2){
  			   if(e2.getStateChange()==2){  			
  		      	  text=(String)contstr+" "+(String)jc.getSelectedItem(); 
  		    	 count.setText(text);
  		    	 finalcounttext=finalcountstr+" "+db.getCount(jc.getSelectedItem().toString());
  		    	 finalcount.setText(finalcounttext);
  		    	 System.out.println(db.getCount(jc.getSelectedItem().toString()));
  		    	 browser.executeJavaScript(  		    			 
  		               "map.setCenter({lat: 17.4063545, lng: 78.5035955});");
  		   }
  		   }
  	 }
  			 );    	 
    	       	    	  
    	 panel1.add(satpanel);   
    	 
    	 JButton btnShowDetails = new JButton("Show Details");
    	 btnShowDetails.setBounds(30, 236, 112, 23);
    	 satpanel.add(btnShowDetails);
    	 panel1.add(dispanel);
    	 
    	 
    	
     }

   

     public static void main(String args[]) throws IOException{
    	new Page22();    		
    	    		    
    	        }
     }



