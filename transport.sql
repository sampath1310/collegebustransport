-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2016 at 10:19 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `transport`
--

-- --------------------------------------------------------

--
-- Table structure for table `transport_info`
--

CREATE TABLE `transport_info` (
  `ROUTE__NO` int(11) NOT NULL,
  `DRIVER__ID` varchar(12) DEFAULT NULL,
  `PASSE__ID` varchar(10) NOT NULL,
  `PASSE_NAME` varchar(15) DEFAULT NULL,
  `PASSE_STOP` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transport_info`
--

INSERT INTO `transport_info` (`ROUTE__NO`, `DRIVER__ID`, `PASSE__ID`, `PASSE_NAME`, `PASSE_STOP`) VALUES
(2, 'RAMESH', '13E51A0301', 'MILIN', 'DILSHUKNAGAR'),
(3, 'name', '14E51A000', 'noname', 'nostop'),
(2, 'RAMESH', '14E51A0330', 'REHAN', 'MUSHEERABAD'),
(2, 'RAMESH', '14E51A0412', 'VIMAL', 'KOTI'),
(2, 'RAMESH', '14E51A0527', 'SAITEJA', 'RTCXROADS'),
(2, 'RAMESH', '14E51A0542', 'SAMPATH', 'RTCXROADS'),
(2, 'RAMESH', '14E51A0547', 'VISHNU', 'SECUNDERABAD'),
(2, 'RAMESH', '14E51A05A4', 'SUNIL', 'RTCXROADS'),
(2, 'RAMESH', '15E51A0242', 'ABHINAV', 'GOLKONADXRDS'),
(2, 'RAMESH', '15E51A0400', 'SATISH', 'GANDHINAGAR'),
(2, 'RAMESH', '15E51A0444', 'GAURAV', 'GANDHINAGAR'),
(2, 'RAMESH', '15E51A0520', 'HEMANTH', 'GANDHINAGAR');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `transport_info`
--
ALTER TABLE `transport_info`
  ADD PRIMARY KEY (`PASSE__ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
